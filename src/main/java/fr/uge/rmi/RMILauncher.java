package fr.uge.rmi;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;

import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.employees.server.EmployeeServer;

public class RMILauncher {
	/** Launch all RMI servers (Employee and Car servers)
	 * @throws AlreadyBoundException 
	 * @throws MalformedURLException 
	 * @throws RemoteException **/
	public static void launch() throws RemoteException, MalformedURLException, AlreadyBoundException {
		EmployeeServer.launch();
		CarServer.launch();
	}
	public static void main(String[] args) {
		try {
			launch();
		} catch (RemoteException | MalformedURLException | AlreadyBoundException e) {
			System.err.println("Class: " + RMILauncher.class.getName() + " Problem: "  + e);
		}
	}
}
