package fr.uge.rmi.cars;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class CarsDB {
	private ArrayList<CarEntry> database;
	
	public class CarEntry {
		private long id;
		private long value;
		private boolean isSalable;
		private String model;
		private Mark[] marks;
		
		public long getId() {
			return id;
		}
		
		public String getModel() {
			return model;
		}
		
		public long getValue() {
			return value;
		}
		
		public boolean isSalable() {
			return isSalable;
		}
		
		public Mark[] getMarks() {
			return marks;
		}
	}
	
    /** Instance unique non préinitialisée */
    //private static CarsDB INSTANCE = null;
	
	public static Optional<CarsDB> loadFromJsonFile(Path path) {
	 	File configFile = path.toFile();
		
		if (!configFile.exists()) {
			return Optional.empty();
		}
		
		Gson gson = new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(configFile));
			CarsDB carsDB = (CarsDB)gson.fromJson(reader, CarsDB.class);
			return Optional.of(carsDB);
		} catch (JsonIOException | JsonSyntaxException | IOException e) {
			System.err.println("An error occurs when parsing file : " + configFile + '\n' + e.getCause());
			return Optional.of(null);
		}
	}
	
	public static Optional<CarsDB> loadDefaultConfiguration() {
		Path defaultConfigFilePath = Paths.get("src", "main", "resources", "Cars", "defaultDB.json");
		return loadFromJsonFile(defaultConfigFilePath);
	}
	
	public static Optional<CarsDB> loadFromConfigurationFile(String filename) {
		Path configFilePath = Paths.get("src", "main", "resources", "Cars", filename);
		return loadFromJsonFile(configFilePath);
	}
	
	public List<CarEntry> getAllData(){
		return database.stream()
				.collect(Collectors.toList());
	}
	
}
