package fr.uge.rmi.cars;

import java.io.Serializable;
import java.util.Objects;

public class Mark implements Serializable {
	private final int mark;
	private final String username;
	private final String comment;
	
	public static boolean validMark(int mark) {
		if(mark < 0 || mark > 5) {
			return false;
		}
		return true;
	}
	
	public Mark(int mark, String username, String comment) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(comment);
		if(!validMark(mark)) {
			throw new IllegalArgumentException("mark must be between 0 and 10");
		};
		this.mark = mark;
		this.username = username;
		this.comment = comment;
	}
	
	@Override
	public String toString() {
		return "\"" + mark + "/5 " + comment + "\" from " + username;
	}
	
	public String getFullComment() {
		return "<< "+ mark + "/5 " + comment + " >> from " + username;		
	}	
}
