package fr.uge.rmi.cars;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICar extends Remote{
	boolean isModel(String model) throws RemoteException;
	boolean isSalable() throws RemoteException;
	String getModel() throws RemoteException;
	long getValue() throws RemoteException;
	void turnToSalableCar() throws RemoteException;
}
