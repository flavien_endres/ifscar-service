package fr.uge.rmi.cars.server;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.xml.rpc.ServiceException;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.CarsDB;
import fr.uge.rmi.cars.CarsDB.CarEntry;
import fr.uge.rmi.cars.Mark;
import fr.uge.rmi.employees.IEmployee;
import fr.uge.rmi.selling.IfsCars;
import fr.uge.rmi.selling.ifscarclient.IfsCarClientStub;

public class CarServer extends UnicastRemoteObject implements ICarServer, Serializable {
	//private long token;
	private HashMap<Long, Car> database2 = new HashMap<>();
	private HashMap<Car, ArrayList<Mark>> carsMarks= new HashMap<>();
	/** services to alert when a new car is salable **/
	//private ArrayList<IfsCars> servicesToAlert = new ArrayList<IfsCars>();
	/** current rentals **/
	private HashMap<IEmployee, Car> currentRentals = new HashMap<>();
	/** employees waiting for rentals **/
	private HashMap<Car, LinkedList<IEmployee>> waitingRentals = new HashMap<>();
	private ArrayList<Car> waitingReturnForSell = new ArrayList<>();
	
	private fr.uge.rmi.selling.ifscarclient.IfsCars ifsCarsClientStub;
	
	private static Logger logger = Logger.getLogger(CarServer.class.getName());
	
	public CarServer() throws RemoteException {
	}
	
	public boolean getAllData(CarsDB db) {
		Objects.requireNonNull(db);
		List<CarEntry> data = db.getAllData();
		data.forEach(carEntry -> {
			Car car = new Car(carEntry.getId(), carEntry.getValue(),carEntry.getModel(), carEntry.isSalable());
			database2.put(carEntry.getId(), car);					
			List<Mark> marksOfCar = carsMarks.computeIfAbsent(car, c ->  new ArrayList<Mark>());
			marksOfCar.addAll(Arrays.asList(carEntry.getMarks()));
		});
		return true;
	}
	
	private boolean addRental(Car car, IEmployee employee) {
		Car employeeCar = currentRentals.get(employee);
		if (car.equals(employeeCar)) { // the employee already rent the car
			return false;
		}
		LinkedList<IEmployee> waitingEmployees = waitingRentals.computeIfAbsent(car, c ->  new LinkedList<IEmployee>());
		if (waitingEmployees.contains(employee)) { // the employee is already waiting for this car
			return false;
		}
		waitingEmployees.add(employee);
		return true;
	}
	
	@Override
	public boolean rentCarById(long id, IEmployee employee) throws RemoteException {
		if (employee == null) {
			return false;
		}
		Car car = database2.get(id);
		if (car == null) {
			return false;
		}
		boolean returnValue = addRental(car, employee);
		updateCarStatus(car);
		return returnValue;
	}

	@Override
	public boolean rentCarByModel(String model, IEmployee employee) throws RemoteException {
		Collection<Car> allCars = database2.values();
		for (Car car : allCars) {
			if(car.isModel(model) && addRental(car, employee)) {
				updateCarStatus(car);
				return true;
			}
		}
		return false;
	}
	
	private void updateCarStatus(Car car) {
		LinkedList<IEmployee> waitingForRent = waitingRentals.get(car);
		if(waitingForRent == null || waitingForRent.isEmpty()) {
			return;
		}
		System.out.println("looking for new rent");
		IEmployee employee = waitingForRent.poll();
		try {
			if(!employee.alertDisponibility(car)) { // the user didn't take the car we go to the next user
				updateCarStatus(car);
			}
			else {
				currentRentals.put(employee, car);
			}
		} catch (RemoteException e) {
			logger.warning("Remote Exception at updateCarStatus in employee method " + e.getCause());
			//System.err.println("Remote Exception at updateCarStatus in employee method " + e.getCause());
			return;
		}
	}

	private Car releaseCar(IEmployee employee) {
		if (employee == null) {
			return null;
		}
		
		return currentRentals.remove(employee);
	}
	
	@Override
	public void returnCar(IEmployee employee) throws RemoteException {
		Car car = releaseCar(employee);
		if (car == null) {
			return;
		}
		if (!car.isSalable()) {
			car.turnToSalableCar();
		}
		else if(waitingReturnForSell.contains(car)) { // et stub not null ? p-e pas vu que il faut stub pour waiting rempli
			if (ifsCarsClientStub != null) {
				ifsCarsClientStub.readyForDelivery(0, new fr.uge.rmi.selling.ifscarclient.Car("", car.getId(), car.getModel(), true, car.getValue()));
			}
			return;
		}
		updateCarStatus(car);
	}
	
	@Override
	public void returnCar(IEmployee emp, int mark, String comment) throws RemoteException {
		Car car = releaseCar(emp);
		if (car == null) {
			return;
		}
		ArrayList<Mark> marks = carsMarks.computeIfAbsent(car, c -> new ArrayList<Mark>());
		
		marks.add(new Mark(mark, emp.getUsername(), comment));
		car.turnToSalableCar();
		updateCarStatus(car);
	}
	
	@Override
	public boolean sellCar(Car car) throws RemoteException {
		 //Optional<Long> id = database2.entrySet().stream().filter(e -> e.getValue().equals(car)).map(e -> e.getKey()).findAny();
		 Optional<Long> id = database2.entrySet().stream().filter(e -> e.getValue().equals(car)).map(e -> e.getKey()).findAny();
		 // la car a l'id mtn
		 
		 if (id.isPresent()) {
			database2.remove(id.get());
			waitingRentals.remove(car);
			return true;
		}
		 return false;
	}
	
	@Override
	public Car getCar(long id) {
		return database2.get(id);
	}

	@Override
	public boolean isAvailable(long id) {
		Car car = database2.get(id);
		if (car == null) {
			return false;
		}
		return !currentRentals.containsValue(car);
	}
	
	@Override
	public Car[] getSaleCars() throws RemoteException {
		Car[] c = database2.values().stream().filter(Car :: isSalable).collect(Collectors.toList()).toArray((new Car[0]));
		return c;
	}
	
	@Override
	public Car[] getAllCars() throws RemoteException {
		return database2.values().toArray(new Car[database2.size()]);
	}

	@Override
//	public void subscribe(IfsCars carsService) throws RemoteException {
	public void subscribe(long token) throws RemoteException {
		System.out.println("SUBSCRIBE");
		if(ifsCarsClientStub == null) {
			//this.token = token;
			try {
				ifsCarsClientStub = IfsCarClientStub.getClient();
			} catch (ServiceException e) {
				logger.warning("unable to connect to ifscar service");
			}
		}
	}
	
	@Override
	public Mark[] getcarMarks(Car car) throws RemoteException {
		if (car == null) {
			return null;
		}
		List<Mark> marks = carsMarks.get(car);
		
		if (marks == null) {
			return null;
		}
		return marks.toArray(new Mark[0]);
	}
	
	@Override
	public boolean exists(String model) throws RemoteException {
		return database2.values().stream().filter(c -> c.getModel().equals(model)).findAny().isPresent();
	}
	
	
	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder("Registered cars\n");
		database2.entrySet().forEach(e -> {
			strb.append(e.getValue().getModel()).append('\n');
		});
		return strb.toString();
	}
	
	public static void launch() throws RemoteException, MalformedURLException, AlreadyBoundException {
		LocateRegistry.createRegistry(7778);
		Optional<CarsDB> potentialData = CarsDB.loadDefaultConfiguration();
		if (!potentialData.isPresent()) {
			System.err.println("Unable to load configuration");
			return;
		}
		ICarServer server = new CarServer();
		((CarServer)server).getAllData(potentialData.get());
		Naming.bind("rmi://localhost:7778/CarService", server);
	}
	
	public static ICarServer getClient() throws MalformedURLException, RemoteException, NotBoundException {
		return (ICarServer) Naming.lookup("rmi://localhost:7778/CarService");
	}
	
}
