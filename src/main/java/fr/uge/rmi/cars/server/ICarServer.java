package fr.uge.rmi.cars.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.Mark;
import fr.uge.rmi.employees.IEmployee;

public interface ICarServer extends Remote {
	boolean rentCarById(long id, IEmployee emp) throws RemoteException;
	boolean rentCarByModel(String model, IEmployee emp) throws RemoteException;
	void returnCar(IEmployee employee) throws RemoteException;
	void returnCar(IEmployee emp, int mark, String comment) throws RemoteException;
	boolean sellCar(Car car) throws RemoteException;
	Car[] getSaleCars() throws RemoteException;
	Car[] getAllCars() throws RemoteException;
//	void subscribe(IfsCars carsService) throws RemoteException;
	void subscribe(long token) throws RemoteException;
	Car getCar(long id) throws RemoteException;
	boolean isAvailable(long id) throws RemoteException;
	Mark[] getcarMarks(Car car) throws RemoteException;
	/** only for the web front **/
	boolean exists(String model) throws RemoteException;
}
