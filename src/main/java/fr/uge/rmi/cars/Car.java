package fr.uge.rmi.cars;

import java.io.Serializable;

public class Car implements Serializable {
	
	private long id;
	private String model;
	private double value;
	private boolean isSalable;
	
	public Car(){
		id = -1;
		value = 0;
		model = "";
		isSalable = false;
	}
	
	public Car(long id, double value, String model, boolean isSalable){
		this.id = id;
		this.isSalable = isSalable;
		this.value = value;
		this.model = model;
	}
	
	public boolean isModel(String model) {
		return this.model.equals(model);
	}
	
	public String getModel() {
		return model;
	}
	
	public double getValue() {
		return value;
	}
	
	public long getId() {
		return id;
	}
	
	public boolean isSalable() {
		return isSalable;
	}
	
	public void turnToSalableCar() {
		isSalable = true;
	}

	public String getDetails() {
		return model + " " + value;
	}
	
	@Override
	public String toString() {
		return model + " " + value + " " + (!isSalable  ? "rental " : "salable ") + "car";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(! (obj instanceof Car)) {
			return false;
		}
		Car c = (Car)obj;
		return id == c.id;
	}
}
