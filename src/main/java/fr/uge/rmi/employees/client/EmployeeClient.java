package fr.uge.rmi.employees.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Arrays;

import fr.uge.rmi.cars.server.ICarServer;
import fr.uge.rmi.employees.IEmployee;
import fr.uge.rmi.employees.server.IEmployeeServer;

public class EmployeeClient {
	public static IEmployeeServer getClient() throws IOException, RemoteException, NotBoundException {
		return (IEmployeeServer) Naming.lookup("rmi://localhost:7777/EmployeeService");
	}
	
	public static void main(String[] args) {
		
		try {
			System.out.println("Client");
			IEmployeeServer employeeServer = (IEmployeeServer) Naming.lookup("rmi://localhost:7777/EmployeeService");
			ICarServer carServer = (ICarServer) Naming.lookup("rmi://localhost:7778/CarService");
			
			IEmployee employee = employeeServer.connect("Jason_Todd", "12345");
			IEmployee employee2 = employeeServer.connect("Jason_Todd", "12345");
			IEmployee employee3 = employeeServer.connect("Dexter_Morgan", "Rita");
			IEmployee employee4 = employeeServer.connect("Dexter_Morgan2", "AAA");
			System.out.println("must be not null " + employee);
			System.out.println(employee.equals(employee2));
			System.out.println(employee + " " + employee2 + " " + employee3 + " " + employee4);
			System.out.println("rentCarById employee" + carServer.rentCarById(1, employee));
			System.out.println("rentCarById employee2" + carServer.rentCarById(1, employee2));
			System.out.println("rentCarById employee3" + carServer.rentCarByModel("Aston Martin", employee3));
			System.out.println("rentCarById employee4" + carServer.rentCarById(1, employee4));
			System.out.println("rent car emp" + employee.returnCar(carServer));
			System.out.println("rent car emp" + employee.returnCar(carServer));
			//System.out.println("rent car emp3" + employee3.returnCar(carServer));
			System.out.println("size " + carServer.getSaleCars().length);
			System.out.println(Arrays.toString(carServer.getSaleCars()));
			while (true) {
				;
			}
			
		} catch (Exception e) {
			System.err.println("Class: " + EmployeeClient.class.getName() + " Problem: "  + e);
		}
	}
}


/*
new Thread(() -> {
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e1) {
		return;
	}
	try {
		IEmployeesServer employeeServer2 = (IEmployeesServer) Naming.lookup("rmi://localhost:7777/EmployeeService");
		
		IEmployee emp = (IEmployee)employeeServer2.connect("Jason_Todd", "12345");
		
		while (true) {
			Thread.sleep(1000);
		}
	} catch (RemoteException e) {
		return;
	} catch (InterruptedException e) {
		return;
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NotBoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}).start();

while (true) {
	System.out.println(employee.getLala());
	Thread.sleep(5000);
	employee.setLala(1500);
}
*/

