package fr.uge.rmi.employees.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import fr.uge.rmi.employees.IEmployee;

public interface IEmployeeServer extends Remote {
	IEmployee connect(String username, String password) throws RemoteException;
}
