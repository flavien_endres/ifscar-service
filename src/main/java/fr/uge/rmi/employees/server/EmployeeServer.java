package fr.uge.rmi.employees.server;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

import fr.uge.rmi.employees.Employee;
import fr.uge.rmi.employees.EmployeesDB;

public class EmployeeServer extends UnicastRemoteObject implements IEmployeeServer{

	private ArrayList<Employee> database = new ArrayList<>();
	
	private static Logger logger = Logger.getLogger(EmployeeServer.class.getName());
	
	public EmployeeServer() throws RemoteException {
		super();
	}
	
	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder("Registered employees\n");
		database.forEach(emp -> {
			strb.append(emp).append('\n');
		});
		return strb.toString();
	}
	
	@Override
	public Employee connect(String username, String password) throws RemoteException{
		Employee employee = new Employee(username, password);
		int index =  database.indexOf(employee);
		
		if (index == -1) {
			return null;
		}
		return database.get(index);
	}
	
	public void getAllData(EmployeesDB db) {
		Objects.requireNonNull(db);
		database.addAll(db.getDatabase());
	}
	
	public static IEmployeeServer getClient() throws IOException, RemoteException, NotBoundException {
		return (IEmployeeServer) Naming.lookup("rmi://localhost:7777/EmployeeService");
	}
	
	public static void launch() throws RemoteException, MalformedURLException, AlreadyBoundException {
		LocateRegistry.createRegistry(7777);
		Optional<EmployeesDB> potentialData = EmployeesDB.loadDefaultConfiguration();
		if (!potentialData.isPresent()) {
			System.err.println("Unable to load configuration");
			return;
		}
		IEmployeeServer server = new EmployeeServer();
		((EmployeeServer)server).getAllData(potentialData.get());
		Naming.bind("rmi://localhost:7777/EmployeeService", server);
	}
}
