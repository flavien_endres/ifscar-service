package fr.uge.rmi.employees;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import fr.uge.rmi.employees.server.EmployeeServer;

public class EmployeesDB {
	
	/*
	public class SampleData {
		private final String username;
		private final String password;
		
		public SampleData(String username, String password) {
			super();
			this.username = username;
			this.password = password;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}
	}
	*/

	private final ArrayList<Employee> database = new ArrayList<>();
	
	private static Optional<EmployeesDB> loadFromJsonFile(Path p) {
		File configFile = p.toFile();
		
		if (!configFile.exists()) {
			return Optional.empty();
		}
		
		Gson gson = new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(configFile));
			EmployeesDB employeesDB = (EmployeesDB)gson.fromJson(reader, EmployeesDB.class);
			return Optional.of(employeesDB);
		} catch (JsonIOException | JsonSyntaxException | IOException e) {
			System.err.println("An error occurs when parsing file : " + configFile + '\n' + e.getCause());
			return Optional.of(null);
		}
	}
	
	public static Optional<EmployeesDB> loadDefaultConfiguration() {
		Path defaultConfigFile = Paths.get("src", "main", "resources", "Employees", "defaultDB.json");
		return loadFromJsonFile(defaultConfigFile);
	}
	
	public static Optional<EmployeesDB> loadFromConfigurationFile(String filename) {
		Path configFile = Paths.get("src", "main", "resources", "Employees", filename);
		return loadFromJsonFile(configFile);
	}
	
	public static Optional<EmployeeServer> loadFromDB() {
		return Optional.empty();
	}
	
	public static boolean saveCurentDB(EmployeeServer server,Path p) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
//		new FileWriter(Paths.get("src", "main", "resources", "Employees", "test.json"))
		try (Writer writer = new FileWriter(p.toFile())) {
		    gson.toJson(server, writer);
		} catch (JsonIOException | IOException e) {
			System.err.println("An error occurs while writing the file " + e.getCause());
		}
		return true;
	}
	
	/*
	public List<Employee> getDatabase(){
		return database.stream().map(x -> {
			try {
				return new Employee(x.getUsername(), x.getPassword());
			} catch (RemoteException e) {
				System.err.println("Error getDatabase() in " + this.getClass().getName() + " " + e.getCause());
				throw new IllegalArgumentException();
			}
		}).collect(Collectors.toList());
	}
	*/
	
	public List<Employee> getDatabase(){
		return database.stream()
				.collect(Collectors.toList());
	}
}
