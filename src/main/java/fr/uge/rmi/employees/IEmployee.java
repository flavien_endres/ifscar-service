package fr.uge.rmi.employees;

import java.rmi.Remote;
import java.rmi.RemoteException;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.server.ICarServer;

public interface IEmployee extends Remote{
	String getUsername() throws RemoteException;
	
	/** Alerts an employee of the availability of the car<br>
	 * @param car The car which is available, null if the car will never be available again
	 * @return False if the Employee didn't take the car<br>
	 * 		   True otherwise
	**/
	boolean alertDisponibility(Car car) throws RemoteException;
	boolean returnCar(ICarServer carServer) throws RemoteException;
	boolean returnCar(ICarServer carServer, int mark, String comment) throws RemoteException;
	Car getCurrentCar() throws RemoteException;
}


/** Alerts an employee of the availability of the car<br>
 *  Also alert an employee when a car (for which he registered) will never be available again<br>
 * @param car The car which is available, null if the car will never be available again
 * @param neverBeAvailable True if the car will no longer been available ( the car is sold )
 * 		  false otherwise
**/
//boolean alertDisponibility(ICar car, boolean neverBeAvailable) throws RemoteException;
