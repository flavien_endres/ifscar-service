package fr.uge.rmi.employees;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Objects;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.server.ICarServer;

public class Employee extends UnicastRemoteObject implements IEmployee, Serializable{
	
	private String username;
	private String password;
	
	private Car currentCar;
	
	public Employee() throws RemoteException {
		super();
	}
	
	public Employee(String username, String password) throws RemoteException {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		this.username = username;
		this.password = password;
	}

	@Override
	public int hashCode() {
		return username.hashCode() ^ password.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Employee)) {
			return false;
		}
		Employee employee = (Employee)obj;
		return username.equals(employee.username)
				&& password.equals(employee.password);
	}
	
	@Override
	public String toString() {
		return username;
	}
	
	@Override
	public String getUsername() throws RemoteException {
		return username;
	}
	
	@Override
	public boolean alertDisponibility(Car car) throws RemoteException {
		System.out.println(username + " alertDisponibility car == null " + (currentCar == null));
		if (currentCar != null) {
			return false;
		}
		System.out.println("Car availaible!!!");
		currentCar = car;
		return true;
	}
	
	@Override
	public boolean returnCar(ICarServer carServer, int mark, String comment) throws RemoteException {
		System.out.println(username + " returnCar car == null " + (currentCar == null));
		if (currentCar == null) {
			return false;
		}
		carServer.returnCar(this, mark, comment);
		currentCar = null;
		return true;
	}
	
	@Override
	public boolean returnCar(ICarServer carServer) throws RemoteException {
		if (currentCar == null || carServer == null) {
			return false;
		}
		
		carServer.returnCar(this);
		currentCar = null;
		return true;
	}
	
	@Override
	public Car getCurrentCar() {
		return currentCar;
	}
}
