package fr.uge.rmi.web;

import fr.uge.rmi.employees.IEmployee;
import fr.uge.rmi.employees.server.EmployeeServer;

public class Utils {
	public static IEmployee getIEmployee(String username, String password) {
		if (username == null || password == null) {
			return null;
		}
		try {
			return EmployeeServer.getClient().connect(username, password);
		} catch (Exception e) {
			return null;
		}
	}
}
