package fr.uge.rmi.web;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import fr.uge.rmi.selling.ifscarclient.Customer;
import fr.uge.rmi.selling.ifscarclient.IfsCarClientStub;
import fr.uge.rmi.selling.ifscarclient.IfsCars;

public class CartActionServlet extends HttpServlet{
	
	private static final Logger logger = Logger.getLogger(CartActionServlet.class.getName());
	
	private void add(String username, String password, long id, IfsCars client, HttpServletResponse resp) throws RemoteException, IOException {
		if(client.addCarToCart(username, password, id)) {
			resp.setStatus(200);
			resp.getWriter().print("the car was successfully had to cart");
		}
		else {
			resp.setStatus(404);
			resp.getWriter().print("the car is sold");
		}
	}
	
	private void remove(String username, String password, long id, IfsCars client, HttpServletResponse resp) throws RemoteException, IOException {
		if(client.removeCarToCart(username, password, id)) {
			resp.setStatus(200);
			resp.getWriter().print("the car was successfully removed to cart");
		}
		else {
			resp.setStatus(404);
			resp.getWriter().print("empty cart");
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = null;
		String password = null;
		for (Cookie l : req.getCookies()) {
			logger.info(l.getName() + " " + l.getValue());
			if (l.getName().equals("Username")) {
				username  = l.getValue();
			}
			else if (l.getName().equals("Password")) {
				password = l.getValue();
			}
		}
		
		if (username == null || password == null) {
			resp.setStatus(403);
			resp.getWriter().print("cookie error");
			return;
		}
		
		try {
			long id = -1;
			id = Long.parseLong(req.getParameter("car-id"));
			IfsCars client = IfsCarClientStub.getClient();
			Customer customer = client.connect(username, password);
			if (customer == null) {
				resp.setStatus(403);
				resp.getWriter().print("custumer not found");
				return;
			}
			
			String mode = req.getParameter("mode");
			
			if (mode == null) {
				resp.setStatus(403);
				resp.getWriter().print("no action specified");
				return;
			}
			if (mode.equals("add")) {
				logger.info("add");
				add(username, password, id, client, resp);
			}
			else {
				logger.info("remove");
				remove(username, password, id, client, resp);
			}
		} catch (ServiceException e1) {
			resp.setStatus(403);
			resp.getWriter().print("ifscars server error");
			return;
		}
		catch (NumberFormatException e) {
			resp.setStatus(403);
			resp.getWriter().print("fail to parse car-id");
			return;
		}
	}
}
