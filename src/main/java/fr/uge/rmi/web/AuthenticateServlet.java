package fr.uge.rmi.web;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.cars.server.ICarServer;
import fr.uge.rmi.employees.IEmployee;
import fr.uge.rmi.employees.client.EmployeeClient;
import fr.uge.rmi.selling.ifscarclient.Customer;
import fr.uge.rmi.selling.ifscarclient.IfsCarClientStub;
import fr.uge.rmi.selling.ifscarclient.IfsCars;

public class AuthenticateServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(AuthenticateServlet.class.getName());
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String login = req.getParameter("login");
			String password = req.getParameter("password");
			String assignment = req.getParameter("assignment");
			switch (assignment) {
				case "client":
					IfsCars ifsCarService = IfsCarClientStub.getClient();
					Customer customer = ifsCarService.connect(login, password);
					if (customer == null) {
						resp.getWriter().println("authentification failed");
						resp.setStatus(403);
						return;
					}
					req.setAttribute("IfsCarService", ifsCarService);
					req.setAttribute("Customer", ifsCarService.connect(login, password));
					req.setAttribute("TypeOfUser", assignment);
					resp.setStatus(200);
					Cookie cust_username = new Cookie("Username", login);
					Cookie cust_passord = new Cookie("Password", password);
					resp.addCookie(cust_username);
					resp.addCookie(cust_passord);
					req.getRequestDispatcher("WEB-INF/CustomerIndex.jsp").forward(req, resp);
					break;
				default:
					IEmployee employee = EmployeeClient.getClient().connect(login, password);
					ICarServer carServer = CarServer.getClient();
					if (employee == null) {
						resp.getWriter().println("authentification failed");
						resp.setStatus(403);
						return;
					}
//					req.setAttribute("CarServer", carServer);
//					req.setAttribute("Employee", employee);
//					req.setAttribute("TypeOfUser", assignment);
					Cookie emp_user = new Cookie("Username", login);
					Cookie emp_password = new Cookie("Password", password);
					resp.addCookie(emp_user);
					resp.addCookie(emp_password);
					resp.setStatus(200);
					req.getRequestDispatcher("WEB-INF/EmployeeIndex.jsp").forward(req, resp);
					break;
			}
		} catch (Exception e) {
			resp.getWriter().println("authentication error");
			resp.setStatus(403);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
}
