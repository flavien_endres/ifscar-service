package fr.uge.rmi.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import fr.uge.rmi.selling.ifscarclient.Customer;
import fr.uge.rmi.selling.ifscarclient.IfsCarClientStub;
import fr.uge.rmi.selling.ifscarclient.IfsCars;

public class MessageServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = null;
		String password = null;
		for (Cookie l : req.getCookies()) {
			if (l.getName().equals("Username")) {
				username  = l.getValue();
			}
			else if (l.getName().equals("Password")) {
				password = l.getValue();
			}
		}
		
		if (username == null || password == null) {
			resp.setStatus(403);
			resp.getWriter().print("cookie error");
			return;
		}
		
		try {
			String message = IfsCarClientStub.getClient().getMessage(username, password);
			if (message == null) {
				resp.setStatus(200);
				//resp.getWriter().print("no message");
				return;
			}
			resp.setStatus(200);
			resp.getWriter().print(message);
			return;
		} catch (ServiceException e1) {
			resp.setStatus(403);
			resp.getWriter().print("ifscars server error");
			return;
		}
	}
}
