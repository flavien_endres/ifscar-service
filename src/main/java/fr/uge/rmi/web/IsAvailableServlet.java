package fr.uge.rmi.web;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.cars.server.ICarServer;

public class IsAvailableServlet extends HttpServlet{

	private static final Logger logger = Logger.getLogger(IsAvailableServlet.class.getName());
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		long carId = -1;
		String id = req.getParameter("carId");
		String model = req.getParameter("carModel");
		
		if (model == null) {
			resp.getWriter().println("car model not found");
			resp.sendError(403);
			return;
		}
		try {
			ICarServer carServer;
			carServer = CarServer.getClient();
			
			if (!carServer.exists(model)) {
				//need to refresh front page
				resp.sendError(404);
				return;
			}
			resp.getWriter().println((carServer.isAvailable(carId) ? "Disponible" : "En location"));
			resp.sendError(200);
		} catch (Exception e) {
			resp.getWriter().println("Error on cars server");
			resp.sendError(404);
		}
	}
}
