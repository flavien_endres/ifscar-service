package fr.uge.rmi.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.server.ICarServer;
import fr.uge.rmi.employees.IEmployee;

/**
 * Servlet implementation class EmployeeServlet
 */
public class EmployeeHomeServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ICarServer carServer = (ICarServer) req.getAttribute("CarServer");
			IEmployee employee = (IEmployee) req.getAttribute("Employee");
			if (employee == null || carServer == null) {
				resp.setStatus(403);
				resp.getWriter().println("employee not found");
				return;
			}
			resp.getWriter().println("bonjour " + employee.getUsername());
			resp.getWriter().println(carServer.getSaleCars()[0].getDetails());
			for (Car car : carServer.getAllCars()) {
				resp.getWriter().println(car.getDetails());
			};
			
		} catch (Exception e) {
			resp.setStatus(403);
			resp.getWriter().println("error with employee server");
		}
	}
}
