package fr.uge.rmi.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.cars.server.ICarServer;
import fr.uge.rmi.employees.IEmployee;

public class ReturnCarServlet extends HttpServlet{

	private static final Logger logger = Logger.getLogger(ReturnCarServlet.class.getName());
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = null;
		String password = null;
		String comment = null;
		
		ICarServer carServer;
		try {
			carServer = CarServer.getClient();
		} catch (Exception e) {
			resp.getWriter().print("car server doesn't respond");
			resp.setStatus(404);
			return;
		}
		for (Cookie l : req.getCookies()) {
			logger.info(l.getName() + " " + l.getValue());
			if (l.getName().equals("Username")) {
				username  = l.getValue();
			}
			else if (l.getName().equals("Password")) {
				password = l.getValue();
			}
		}
		if(username == null || password == null) {
			resp.getWriter().print("login required");
			resp.setStatus(403);
			return;
		}
		IEmployee employee = Utils.getIEmployee(username, password);
		if (employee == null) {
			resp.getWriter().println("Authentification failed");
			resp.setStatus(403);
			return;
		}
		Car car = employee.getCurrentCar(); // not null
		boolean returnValue;
		
		comment = req.getParameter("comment");
		if (comment == null) { // send without comment
			returnValue = employee.returnCar(carServer);
		}
		else {
			int mark = Integer.parseInt(req.getParameter("mark"));
			returnValue = employee.returnCar(carServer, mark, comment);
		}
		if (returnValue) {
			resp.getWriter().print("you rendered " +  car.getModel() + " with success");
			resp.setStatus(200);
		}
		else {
			resp.getWriter().print("failure during return operation");
			resp.setStatus(403);
		}
	}
}
