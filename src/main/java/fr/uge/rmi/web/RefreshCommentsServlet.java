package fr.uge.rmi.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.Mark;
import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.cars.server.ICarServer;

public class RefreshCommentsServlet extends HttpServlet{
	
	private static final Logger logger = Logger.getLogger(RefreshCommentsServlet.class.getName());
	
	private void printMark(StringBuilder b, Mark mark) {
		if (mark == null) {
			return;
		}
		b.append("\"").append(mark.getFullComment()).append("\",");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HashMap<Long, Mark[]> carsStatus = new HashMap<>();
		try {
			ICarServer server = CarServer.getClient();
			
			Car[] cars = server.getAllCars();
			
			for (Car car : cars) {
				long id = car.getId();
				carsStatus.put(id, server.getcarMarks(car));
			}
			logger.info("AAA");
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			
			resp.setStatus(200);
			PrintWriter out = resp.getWriter();
			StringBuilder strb = new StringBuilder();
//			out.print("{");
			strb.append('{');
//			logger.info("AAZA");
			for (Entry<Long, Mark[]> entry : carsStatus.entrySet()) { // gson doesn't work in servlet  :(
				strb.append("\"" + entry.getKey() + "\":[");
				boolean l = false;
				for (Mark mark : entry.getValue()) {
					l = true;
					printMark(strb, mark);
				}
				if (l) {
					strb.deleteCharAt(strb.length() - 1);
				}
				strb.append("],");
			}
//			logger.info("AAAA");
//			logger.info("result" + strb.toString());
			strb.deleteCharAt(strb.length() - 1);
			strb.append('}');
			out.print(strb.toString());
			logger.info(out.toString());
			return;
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			resp.getWriter().print("carServer error");
			resp.setStatus(404);
			return;
		}
	}
}
