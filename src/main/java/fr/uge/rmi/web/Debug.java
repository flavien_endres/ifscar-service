package fr.uge.rmi.web;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Random;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Debug extends HttpServlet{

	private static final Logger logger = Logger.getLogger(Debug.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Enumeration<String> attributes= req.getAttributeNames();
		logger.info("debug doGet");
		while (attributes.hasMoreElements()) {
			String element = attributes.nextElement();
			logger.info("attribute " + element);
			resp.getWriter().println("attribute " + element);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.info("debug doPost" + req.getParameterMap().toString());
		Enumeration<String> attributes= req.getAttributeNames();
		logger.info(""+req.getCookies().length);
		String username = null;
		String password = null;
		for (Cookie l : req.getCookies()) {
			logger.info("" + l.getName() + " " + l.getValue());
			if (l.getName().equals("Username")) {
				username  = l.getValue();
			}
			else if (l.getName().equals("Password")) {
				password = l.getValue();
			}
		}
		if (username == null || password == null) {
			logger.info("Cookie not found");
			resp.sendError(403);
		}
		while (attributes.hasMoreElements()) {
			String element = attributes.nextElement();
			logger.info("attribute " + element);
			resp.getWriter().println("attribute " + element);
		}
		
		if((new Random()).nextBoolean()) {
			logger.info("error");
			resp.sendError(403);
		}
		else {
			logger.info("success");
			resp.sendError(200);
		}
	}
}
