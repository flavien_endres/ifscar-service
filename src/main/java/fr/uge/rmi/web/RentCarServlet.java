package fr.uge.rmi.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.cars.server.ICarServer;
import fr.uge.rmi.employees.IEmployee;

public class RentCarServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(RentCarServlet.class.getName());
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String username = null;
		String password = null;
		for (Cookie l : req.getCookies()) {
			logger.info(l.getName() + " " + l.getValue());
			if (l.getName().equals("Username")) {
				username  = l.getValue();
			}
			else if (l.getName().equals("Password")) {
				password = l.getValue();
			}
		}
		IEmployee employee = Utils.getIEmployee(username, password);
		if (employee == null) {
			resp.getWriter().println("Authentification failed");
			resp.setStatus(403);
			return;
		}
		String carIdToRent = req.getParameter("car-id");
		String carNameToRent = req.getParameter("car-model");
		logger.info("carToRent:" + carIdToRent);
		
		if (carIdToRent == null || carNameToRent == null) {
			resp.getWriter().println("Car attributes missing");
			resp.setStatus(403);
			return;
		}
		if (employee.getCurrentCar() != null) {
			resp.getWriter().println("You already rent a car");
			resp.setStatus(403);
			return;
		}
		try {
			ICarServer carServer;
			carServer = CarServer.getClient();
			if (!carServer.exists(carNameToRent)) {
				resp.getWriter().print(carNameToRent + " is no longer available");
				resp.setStatus(404);
				return;
			}
			if (carServer.rentCarByModel(carNameToRent, employee)) {
				resp.getWriter().print("you are in line to rent " + carNameToRent);
				resp.setStatus(200);
				return;
			}
			else {
				resp.getWriter().print("you already rent or are in line to rent " + carIdToRent);
				resp.setStatus(403);
				return;
			}
		} catch (Exception e) {
			resp.getWriter().print("Error on cars server");
			resp.setStatus(403);
		}
	}
}
