package fr.uge.rmi.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.server.CarServer;
import fr.uge.rmi.cars.server.ICarServer;

public class DisponibilityServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(DisponibilityServlet.class.getName());
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HashMap<Long, Integer> carsStatus = new HashMap<>();
		try {
			ICarServer server = CarServer.getClient();
			
			Car[] cars = server.getAllCars();
			
			for (Car car : cars) {
				long id = car.getId();
				carsStatus.put(id, (server.isAvailable(id) ? 1 : 0));
			}
			
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
			
			resp.setStatus(200);
			PrintWriter out = resp.getWriter();
			StringBuilder strb = new StringBuilder();
			strb.append('{');
			for (Entry<Long, Integer> entry : carsStatus.entrySet()) { // gson doesn't work in servlet  :(
				strb.append("\"" + entry.getKey() + "\":" + entry.getValue() + ",");
			}
			strb.deleteCharAt(strb.length() - 1);
			strb.append('}');
			out.print(strb);
			return;
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			resp.getWriter().print("carServer error");
			resp.setStatus(404);
			return;
		}
		
		
	}
}
