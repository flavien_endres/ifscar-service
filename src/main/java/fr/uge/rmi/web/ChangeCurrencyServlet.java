package fr.uge.rmi.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import fr.uge.rmi.selling.ifscarclient.Car;
import fr.uge.rmi.selling.ifscarclient.IfsCarClientStub;
import fr.uge.rmi.selling.ifscarclient.IfsCars;

public class ChangeCurrencyServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HashMap<Long, Double> carsPrice = new HashMap<>();
			
		try {
			IfsCars client = IfsCarClientStub.getClient();
			String currency = req.getParameter("Currency");
			if (currency == null) {
				resp.getWriter().print("Currency parameter missing");
				resp.setStatus(403);
				return;
			}
			Car[] cars = client.getCars(currency);
			for (Car car : cars) {
				carsPrice.put(car.getId(), car.getValue());
			}
			
			StringBuilder strb = new StringBuilder();
			PrintWriter out = resp.getWriter();
			out.append('{');
			for (Entry<Long, Double> entry : carsPrice.entrySet()) {
				strb.append("\"" + entry.getKey() + "\":" + entry.getValue() + ",");
			}
			strb.deleteCharAt(strb.length() - 1);
			strb.append('}');
			out.print(strb);
			resp.setStatus(200);
			resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
		} catch (ServiceException e) {
			resp.getWriter().print("IfsCars server problem");
			resp.setStatus(404);
		}
		
	}
}
