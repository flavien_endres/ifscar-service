package fr.uge.rmi.web;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.selling.ifscarclient.Customer;
import fr.uge.rmi.selling.ifscarclient.IfsCarClientStub;
import fr.uge.rmi.selling.ifscarclient.IfsCars;

public class CustomerHomeServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(CustomerHomeServlet.class.getName());
	private static fr.uge.rmi.selling.ifscarclient.IfsCars service;
	static {
		try {
			service = IfsCarClientStub.getClient();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.getLogger(this.getClass().getName()).info("doGet");
//		response.getWriter().println("jsudfsdf");
//		response.setStatus(200);
		try {
			fr.uge.rmi.selling.ifscarclient.Customer customer = (Customer) request.getAttribute("Customer");
//			fr.uge.rmi.selling.ifscarclient.IfsCars service = (IfsCars) request.getAttribute("IfsCarService");
			Logger.getLogger(this.getClass().getName()).info(request.getParameter("login"));
//			Stream.of(request.getAttributeNames()).forEach(s -> Logger.getLogger(this.getClass().getName()).info(s.));
			/*
			Enumeration<String> l = request.getAttributeNames();
			while (l.hasMoreElements()) {
				logger.info(l.nextElement());
			}
			logger.info("fin");
			*/
//			request.getAttributeNames()
			if (customer == null) {
				response.setStatus(403);
				response.getWriter().println("customer not found");
				return;
			}
			response.setStatus(200);
			response.setContentType("text/html");
			response.getWriter().println("bonjour " + customer.getUsername());
			response.getWriter().println("bonjour " + customer.getUsername());
//			logger.info("bonjour " + customer.getUsername());
			
		} catch (Exception e) {
			response.setStatus(403);
			response.getWriter().println("error with customer server");
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Logger.getLogger(this.getClass().getName()).info("doPost");
		try {
			fr.uge.rmi.selling.ifscarclient.Customer customer = (Customer) req.getAttribute("Customer");
			fr.uge.rmi.selling.ifscarclient.IfsCars service = (IfsCars) req.getAttribute("IfsCarService");
			if (customer == null || service == null) {
				resp.setStatus(403);
				resp.getWriter().println("Attribute not found");
				return;
			}
			resp.setStatus(200);
			resp.getWriter().println("bonjour " + customer.getUsername());
			resp.getWriter().println(service.getCars("EUR")[0].getDetails());
		} catch (Exception e) {
			resp.setStatus(403);
			resp.getWriter().println("error with customer server");
		}
	}

}
