package fr.uge.rmi.selling.bankclient;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class BankClientStub {
	public static Bank getClient() throws ServiceException {
		Bank service = new BankServiceLocator().getBank();
		((BankSoapBindingStub) service).setMaintainSession(true);
		return service;
	}
	
	public static void main(String[] args) throws ServiceException, RemoteException {
		Bank bankService = getClient();
		
		System.out.println(bankService.validatePayment(1781664849903541L, 000));
		System.out.println(bankService.validatePayment(1781664849903541L, 78000D));
		System.out.println(bankService.validatePayment(1781664849903541L, 73000D));
	}
}
