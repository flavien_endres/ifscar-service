package fr.uge.rmi.selling.bankclient;

public class BankProxy implements fr.uge.rmi.selling.bankclient.Bank {
  private String _endpoint = null;
  private fr.uge.rmi.selling.bankclient.Bank bank = null;
  
  public BankProxy() {
    _initBankProxy();
  }
  
  public BankProxy(String endpoint) {
    _endpoint = endpoint;
    _initBankProxy();
  }
  
  private void _initBankProxy() {
    try {
      bank = (new fr.uge.rmi.selling.bankclient.BankServiceLocator()).getBank();
      if (bank != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bank)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bank)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bank != null)
      ((javax.xml.rpc.Stub)bank)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.uge.rmi.selling.bankclient.Bank getBank() {
    if (bank == null)
      _initBankProxy();
    return bank;
  }
  
  public boolean validatePayment(long creditCardNumber, double totalToPay) throws java.rmi.RemoteException{
    if (bank == null)
      _initBankProxy();
    return bank.validatePayment(creditCardNumber, totalToPay);
  }
  
  
}