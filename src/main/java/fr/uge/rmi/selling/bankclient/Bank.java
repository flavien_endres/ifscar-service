/**
 * Bank.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.rmi.selling.bankclient;

public interface Bank extends java.rmi.Remote {
    public boolean validatePayment(long creditCardNumber, double totalToPay) throws java.rmi.RemoteException;
}
