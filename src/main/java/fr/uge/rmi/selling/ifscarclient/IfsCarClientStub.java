package fr.uge.rmi.selling.ifscarclient;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class IfsCarClientStub {
	
	public static void main(String[] args) throws RemoteException, ServiceException {
		IfsCars service = getClient();
		
		System.out.println(service.addCarToCart("Mahdi", "Mahdi", 0));
		System.out.println(service.getCart("Mahdi", "Mahdi"));
	}
	
	public static IfsCars getClient() throws ServiceException {
		IfsCars service = new IfsCarsServiceLocator().getIfsCars();
		((IfsCarsSoapBindingStub)service).setMaintainSession(true);
		return service;
	}
}
