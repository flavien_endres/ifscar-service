/**
 * IfsCarsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.rmi.selling.ifscarclient;

public class IfsCarsServiceLocator extends org.apache.axis.client.Service implements fr.uge.rmi.selling.ifscarclient.IfsCarsService {

    public IfsCarsServiceLocator() {
    }


    public IfsCarsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IfsCarsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IfsCars
    private java.lang.String IfsCars_address = "http://localhost:8080/Ifscars/services/IfsCars";

    public java.lang.String getIfsCarsAddress() {
        return IfsCars_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IfsCarsWSDDServiceName = "IfsCars";

    public java.lang.String getIfsCarsWSDDServiceName() {
        return IfsCarsWSDDServiceName;
    }

    public void setIfsCarsWSDDServiceName(java.lang.String name) {
        IfsCarsWSDDServiceName = name;
    }

    public fr.uge.rmi.selling.ifscarclient.IfsCars getIfsCars() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IfsCars_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIfsCars(endpoint);
    }

    public fr.uge.rmi.selling.ifscarclient.IfsCars getIfsCars(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            fr.uge.rmi.selling.ifscarclient.IfsCarsSoapBindingStub _stub = new fr.uge.rmi.selling.ifscarclient.IfsCarsSoapBindingStub(portAddress, this);
            _stub.setPortName(getIfsCarsWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIfsCarsEndpointAddress(java.lang.String address) {
        IfsCars_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (fr.uge.rmi.selling.ifscarclient.IfsCars.class.isAssignableFrom(serviceEndpointInterface)) {
                fr.uge.rmi.selling.ifscarclient.IfsCarsSoapBindingStub _stub = new fr.uge.rmi.selling.ifscarclient.IfsCarsSoapBindingStub(new java.net.URL(IfsCars_address), this);
                _stub.setPortName(getIfsCarsWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IfsCars".equals(inputPortName)) {
            return getIfsCars();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://selling.rmi.uge.fr", "IfsCarsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://selling.rmi.uge.fr", "IfsCars"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IfsCars".equals(portName)) {
            setIfsCarsEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
