/**
 * IfsCarsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.rmi.selling.ifscarclient;

public interface IfsCarsService extends javax.xml.rpc.Service {
    public java.lang.String getIfsCarsAddress();

    public fr.uge.rmi.selling.ifscarclient.IfsCars getIfsCars() throws javax.xml.rpc.ServiceException;

    public fr.uge.rmi.selling.ifscarclient.IfsCars getIfsCars(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
