package fr.uge.rmi.selling.ifscarclient;

public class IfsCarsProxy implements fr.uge.rmi.selling.ifscarclient.IfsCars {
  private String _endpoint = null;
  private fr.uge.rmi.selling.ifscarclient.IfsCars ifsCars = null;
  
  public IfsCarsProxy() {
    _initIfsCarsProxy();
  }
  
  public IfsCarsProxy(String endpoint) {
    _endpoint = endpoint;
    _initIfsCarsProxy();
  }
  
  private void _initIfsCarsProxy() {
    try {
      ifsCars = (new fr.uge.rmi.selling.ifscarclient.IfsCarsServiceLocator()).getIfsCars();
      if (ifsCars != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ifsCars)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ifsCars)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ifsCars != null)
      ((javax.xml.rpc.Stub)ifsCars)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.uge.rmi.selling.ifscarclient.IfsCars getIfsCars() {
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars;
  }
  
  public fr.uge.rmi.selling.ifscarclient.Car[] getCart(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.getCart(username, password);
  }
  
  public boolean readyForDelivery(long token, fr.uge.rmi.selling.ifscarclient.Car c) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.readyForDelivery(token, c);
  }
  
  public boolean hasMessage(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.hasMessage(username, password);
  }
  
  public fr.uge.rmi.selling.ifscarclient.Car[] getPurchasedCars(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.getPurchasedCars(username, password);
  }
  
  public boolean startPayment(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.startPayment(username, password);
  }
  
  public fr.uge.rmi.selling.ifscarclient.Car[] getCars(java.lang.String currency) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.getCars(currency);
  }
  
  public boolean addCarToCart(java.lang.String username, java.lang.String password, long id) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.addCarToCart(username, password, id);
  }
  
  public boolean isAvailable(fr.uge.rmi.selling.ifscarclient.Car c) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.isAvailable(c);
  }
  
  public java.lang.String getMessage(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.getMessage(username, password);
  }
  
  public fr.uge.rmi.selling.ifscarclient.Customer connect(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.connect(username, password);
  }
  
  public boolean removeCarToCart(java.lang.String username, java.lang.String password, long id) throws java.rmi.RemoteException{
    if (ifsCars == null)
      _initIfsCarsProxy();
    return ifsCars.removeCarToCart(username, password, id);
  }
  
  
}