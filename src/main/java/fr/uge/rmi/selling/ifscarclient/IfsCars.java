/**
 * IfsCars.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.rmi.selling.ifscarclient;

public interface IfsCars extends java.rmi.Remote {
    public boolean removeCarToCart(java.lang.String username, java.lang.String password, long id) throws java.rmi.RemoteException;
    public fr.uge.rmi.selling.ifscarclient.Car[] getCart(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public boolean readyForDelivery(long token, fr.uge.rmi.selling.ifscarclient.Car c) throws java.rmi.RemoteException;
    public boolean hasMessage(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public fr.uge.rmi.selling.ifscarclient.Car[] getPurchasedCars(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public boolean startPayment(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public fr.uge.rmi.selling.ifscarclient.Car[] getCars(java.lang.String currency) throws java.rmi.RemoteException;
    public boolean addCarToCart(java.lang.String username, java.lang.String password, long id) throws java.rmi.RemoteException;
    public boolean isAvailable(fr.uge.rmi.selling.ifscarclient.Car c) throws java.rmi.RemoteException;
    public java.lang.String getMessage(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public fr.uge.rmi.selling.ifscarclient.Customer connect(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
}
