package fr.uge.rmi.selling;


import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.xml.rpc.ServiceException;

import com.currencysystem.webservices.currencyserver.CurncsrvReturnRate;
import com.currencysystem.webservices.currencyserver.CurrencyClientStub;
import com.currencysystem.webservices.currencyserver.CurrencyServerSoap;

import fr.uge.rmi.cars.Car;
import fr.uge.rmi.cars.server.ICarServer;
import fr.uge.rmi.selling.bankclient.Bank;
import fr.uge.rmi.selling.bankclient.BankClientStub;

public class IfsCars {
	/** Useful to ensure that the method call comes from the carServer **/
	private final long token = 7069245004927494604L;
	
	/** Cart of all customer **/
	private HashMap<Customer, Set<Car>> carts = new HashMap<>();
	
	/** Messages sent to a customer <br>
	 *  e.g: payment failure, car no longer available, car shipped...<br>
	**/
	private HashMap<Customer, ArrayList<String>> customerNotification = new HashMap<Customer, ArrayList<String>>();
	
	/** All cars purchased by the customer from the service **/
	private HashMap<Customer, Set<Car>> purchasedCars = new HashMap<Customer, Set<Car>>();
	
	/** Purchased cars that was not available when they was sell<br>
	 *  We will notify the user when it would be shipped 
	**/
	private HashMap<Car, Customer> waitingForDelivery = new HashMap<Car, Customer>();
	
	/** The server containing the company's cars **/
	private ICarServer carServer;
	/** The server for currency exchange **/
	private CurrencyServerSoap currencyService;
	/** The bank to validate the customer's payment **/
	private Bank bankService;
	
	private final ArrayList<Customer> customers;
	
	private static final Logger logger = Logger.getLogger(IfsCars.class.getName());
	
	public IfsCars() {
		customers = new ArrayList<Customer>() {{
	        add(new Customer("Bruce_Wayne", "Thomas_Martha", 4502551244645585L));
	        add(new Customer("Theon_Greyjoy", "Reek", 8750937287339063L));
	        add(new Customer("Ramsay_Bolton", "Sansa", 6521618819225328L));
	        add(new Customer("Elliot_Alderson", "Moss178.@ygz", 8198007807215522L));
	        add(new Customer("Mahdi", "Mahdi", 1781664849903541L));
	    }};
	}

	public Customer connect(String username, String password) {
		return getCustomerWithId(username, password, "connect");
	}
	
	private void setCarServer() throws RemoteException, MalformedURLException, NotBoundException {
		if (carServer != null) {
			return;
		}
		carServer = (ICarServer) Naming.lookup("rmi://localhost:7778/CarService");
		carServer.subscribe(token);
	}
	
	private void setBankService() throws ServiceException {
		bankService = BankClientStub.getClient();
	}
	
	public boolean startPayment(String username, String password) throws RemoteException {
		try {
			setCarServer();
			setBankService();
		} catch (Exception e) {
			return false;
		}
		Customer customer = getCustomerWithId(username, password, "startPayment");
		if (customer == null) {
			return false;
		}
		Set<Car> cart = carts.get(customer);
		ArrayList<String> messages = customerNotification.get(customer);
		if(messages == null) {
			messages = new ArrayList<String>();
			customerNotification.put(customer, messages);
		}
		if (cart == null) {
			messages.add("empty cart");
			return false;
		}
		boolean returnValue = true;
		for (Car car : cart) {
			if (carServer.getCar(car.getId()) == null) {
				messages.add("Car id:" + car.getId() + " model:" + car.getModel() + " is no longer available");
				returnValue =  false;
			}
		}
		if (!returnValue) {
			return false;
		}
		double cartValue = 0;
		for (Car car : cart) {
			cartValue += car.getValue();
		}
		returnValue = bankService.validatePayment(customer.getCreditCardNumber(), cartValue);
		if (returnValue) {
			Set<Car> customerCollection = purchasedCars.get(customer);
			if (customerCollection == null) {
				customerCollection = new HashSet<Car>();
				purchasedCars.put(customer, customerCollection);
			}
			for (Car car : cart) {
				if (carServer.sellCar(car)) {
					messages.add("Car " + car.getModel() + " has been shipped");
					customerCollection.add(car);
					cart.remove(car);
				}
				else {
					messages.add("Car " + car.getModel() + " will be shipped when available");
					waitingForDelivery.put(car, customer);
				}
			}
		}
		else {
			messages.add("The bank refused the operation");
		}
		return returnValue;
	}
	
	private void setCurrencyService() throws ServiceException {
		currencyService = CurrencyClientStub.getCurrencyServerSoap();
	}
	
	public Car[] getCars(String currency) throws RemoteException {
		try {
			setCurrencyService();
			setCarServer();
		} catch (Exception e) {
			return new Car[0];
		}
		currencyService.activeCurrencies("").split(";");
		if(!currencyService.currencyExists("", currency, false)) {
			return new Car[0];
		}
		//return cars.toArray(new Car[cars.size()]);
		Car[] cars = carServer.getSaleCars();
		//System.out.println(Arrays.toString(cars));
		if (currency.equals("EUR")) {
			return cars;
		}
		ArrayList<Car> carsWithRightCurrency = new ArrayList<Car>();
		
		for (Car car : cars) {
			double newValue = (double) currencyService.convert("", "EUR", currency, car.getValue(), true, "", CurncsrvReturnRate.curncsrvReturnRateNumber, "", "");
			carsWithRightCurrency.add(new Car(car.getId(), newValue, car.getModel(), true));
		}
		
		return carsWithRightCurrency.toArray(new Car[carsWithRightCurrency.size()]);
	}
	
	private Customer getCustomerWithId(String username, String password, String functionName) {
		if (username == null || password == null) {
			return null;
		}
		int index = customers.indexOf(new Customer(username, password));
		if (index == -1) {
//			logger.info(functionName + " customer " + username + " " + password + " not found");
			return null;
		}
		return customers.get(index);
	}
	
	public boolean addCarToCart(String username, String password, long id) throws RemoteException {
		try {
			setCarServer();
		} catch (Exception e) {
			return false;
		}
		Customer customer = getCustomerWithId(username, password, "addCarToCart");
		if (customer == null) {
			return false;
		}
		logger.info("addCarToCart customer :" + customer);
		Car c = carServer.getCar(id);
		if (c == null || !c.isSalable()) {
			logger.info("pas vendable customer :" + customer);
			return false;
		}
		Set<Car> cart = carts.get(customer);
		if (cart == null) {
			cart = new HashSet<Car>();
			carts.put(customer, cart);
		}
		logger.info("ajout dans cart :" + customer);
		cart.add(c);
		return true;
	}
	
	public boolean removeCarToCart(String username, String password, long id) throws RemoteException {
		Customer customer = getCustomerWithId(username, password, "removeCarToCart");
		if (customer == null) {
			logger.warning("removeCarToCart: " + "customer null");
			return false;
		}
		Set<Car> cart = carts.get(customer);
		if (cart == null) {
			logger.warning("removeCarToCart: " + "empty cart");
			return false;
		}
		cart.remove(new Car(id, 0, "", true));
		return true;
	}
	
	public Car[] getCart(String username, String password) {
		Customer customer = getCustomerWithId(username, password, "getCart");
		if (customer == null) {
			return null;
		}
		Set<Car> cart = carts.get(customer);
		logger.info(customer.toString());
		if (cart == null) {
			logger.info(customer.toString() + " empty cart");
			return new Car[0];
		}
		return cart.toArray(new Car[0]);
	}
	
	public boolean readyForDelivery(long token, Car c) {
		if (this.token != token) {
			return false;
		}
		Customer customer = waitingForDelivery.get(c);
		 
		if (customer == null) {
			return false;
		}
		ArrayList<String> messages = customerNotification.get(customer);
		if (messages == null) { // impossible
			messages = new ArrayList<String>();
		}
		messages.add("Car " + c.getModel() + "  is ready !");
		Set<Car> tmp = purchasedCars.get(customer);
		if (tmp == null) {
			tmp = new HashSet<Car>();
		}
		tmp.add(c);
		waitingForDelivery.remove(c);
		return true;
	}
	
	public boolean isAvailable(Car c) throws RemoteException {
		if (c == null) {
			return false;
		}
		try {
			setCarServer();
		} catch (Exception e) {
			return false;
		}
		return carServer.isAvailable(c.getId());
	}
	
	public String getMessage(String username, String password) {
		Customer customer = getCustomerWithId(username, password, "getMessage");
		if (customer == null) {
			return null;
		}
		ArrayList<String> messages = customerNotification.get(customer);
		if (messages == null || messages.isEmpty()) {
			return null;
		}
		return messages.remove(0);
	}
	
	public boolean hasMessage(String username, String password) {
		Customer customer = getCustomerWithId(username, password, "hasMessage");
		if (customer == null) {
			return false;
		}
		ArrayList<String> messages = customerNotification.get(customer);
		if (messages == null) {
			return false;
		}
		return messages.size() != 0;
	}
	
	public Car[] getPurchasedCars(String username, String password) {
		Customer customer = getCustomerWithId(username, password, "getPurchasedCars");
		if (customer == null) {
			return null;
		}
		Set<Car> cars = purchasedCars.get(customer);
		if (cars == null) {
			return new Car[0];
		}
		return cars.toArray(new Car[0]);
	}
}
