package fr.uge.rmi.selling;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClientServer extends Remote {
	Customer connect(String username, String password) throws RemoteException;
}
