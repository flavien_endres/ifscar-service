package fr.uge.rmi.selling;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class ClientDB implements Serializable {
	private static ArrayList<Customer> database;

	public ClientDB() {
	}
	
	{
		database = new ArrayList<Customer>();
		database.addAll(Arrays.asList(loadDefaultConfiguration().getAllData()));
	}
	
	public static ClientDB loadFromJsonFile(Path path) {
	 	File defaultConfigFile = path.toFile();
		
		if (!defaultConfigFile.exists()) {
			return null;
		}
		
		Gson gson = new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(defaultConfigFile));
			ClientDB clientDB = (ClientDB)gson.fromJson(reader, ClientDB.class);
			return clientDB;
		} catch (JsonIOException | JsonSyntaxException | IOException e) {
			System.err.println("An error occurs when parsing file : " + defaultConfigFile + '\n' + e.getCause());
			return null;
		}
	}
	
	public static Customer[] lala() {
		return database.toArray(new Customer[database.size()]);
	}
	
	public static ClientDB loadDefaultConfiguration() {
		Path defaultConfigFile = Paths.get("src", "main", "resources", "Clients", "defaultDB.json");
		return loadFromJsonFile(defaultConfigFile);
	}
	
	public static ClientDB loadFromConfigurationFile(String filename) {
		Path configFile = Paths.get("src", "main", "resources", "Clients", filename);
		return loadFromJsonFile(configFile);
	}
	
	public Customer[] getAllData(){
		return database.toArray(new Customer[database.size()]);
	}
	
	public static void main(String[] args) {
		System.out.println(Arrays.toString(ClientDB.loadDefaultConfiguration().getAllData()));
	}
}
