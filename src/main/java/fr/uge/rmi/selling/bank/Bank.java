package fr.uge.rmi.selling.bank;

import java.util.HashMap;
import java.util.logging.Logger;

public class Bank {
	// credit card number / balance
	private final HashMap<Long, Double> accounts = new HashMap<Long, Double>() {{
	    put(4502551244645585L, 690982040D);
		put(8750937287339063L, 206362093D);
		put(6521618819225328L, 275997465D);
		put(8198007807215522L, 588093683D);
		put(1781664849903541L, 75000D);
		put(9386070741109792L, 100009452D);
		put(9720291407259111L, 757532444D);
	    put(6812215329736498L, 450957929D);
	    put(7476026089655985L, 284197739D);
	    put(1936884890306358L, 240863699D);
	    put(9302330219242136L, 448614404D);
	}};
	
	private static Logger logger = Logger.getLogger(Bank.class.getName());
	
	public Bank() {
	}
	
	public boolean validatePayment(long creditCardNumber, double totalToPay) {
		Double balance = accounts.get(creditCardNumber);
		
		//logger.info("balance of " + creditCardNumber + " : " + accounts.get(creditCardNumber));
		if (balance == null || balance < totalToPay) {
			return false;
		}
		balance -= totalToPay;
		accounts.replace(creditCardNumber, balance);
		logger.info("new balance of " + creditCardNumber + " : " + accounts.get(creditCardNumber));
		return true;
	}
	
	/*
	// TODO remove
	public double getSolde(long creditCardNumber) {
		return accounts.getOrDefault(creditCardNumber, 0D); 
	}
	*/
}
