package fr.uge.rmi.selling;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;

import fr.uge.rmi.cars.server.CarServer;

public class ClientServer extends UnicastRemoteObject implements IClientServer {
	private ArrayList<Customer> database;
	
	public ClientServer(Customer[] client) throws RemoteException {
		database.addAll(Arrays.asList(client));
	}
	
	@Override
	public Customer connect(String username, String password) throws RemoteException {
		int index = database.indexOf(new Customer(username, password, 0));
		if (index == -1) {
			return null;
		}
		return database.get(index);
	}
	
	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(7779);
			ClientDB potentialData = ClientDB.loadDefaultConfiguration();
			if (potentialData == null) {
				System.err.println("Unable to load configuration");
				return;
			}
			IClientServer server = new ClientServer(potentialData.getAllData());
			Naming.bind("rmi://localhost:7779/ClientDBService", server);
			System.out.println(server);
		} catch (Exception e) {
			System.err.println("Class: " + CarServer.class.getName() + " Problem: "  + e);
		}
	}
}
