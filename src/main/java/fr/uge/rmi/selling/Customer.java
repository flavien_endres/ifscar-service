package fr.uge.rmi.selling;

import java.io.Serializable;

public class Customer implements Serializable{
	private long creditCardNumber;
	private String username;
	private String password;
	
	public Customer() {
		creditCardNumber = 0L;
		username = password = "";
	}
	
	public Customer(String username, String password, long creditCardNumber) {
		this.username = username;
		this.password = password;
		this.creditCardNumber = creditCardNumber;
	}
	
	public Customer(String username, String password) {
		this(username, password, 0);
	}
	
	public long getCreditCardNumber() {
		return creditCardNumber;
	}

	@Override
	public int hashCode() {
		return password.hashCode() ^ username.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Customer)) {
			return false;
		}
		return equality((Customer) obj);
	}
	
	private boolean equality(Customer customer) {
		if (customer == null) {
			return false;
		}
		Customer client = (Customer)customer;
		return username.equals(client.username)
				&& password.equals(client.password);
	}
	
	public String getUsername() {
		return username;
	}
	
	// safe only the user and the server can access this method
	// we must pass by the connect() method to get the real client
	public String getPassword() {
		return password;
	}
	
	public boolean isEquals(Customer customer) {
		return equality(customer);
	}

	@Override
	public String toString() {
		return username + " " + password;
	}
}
