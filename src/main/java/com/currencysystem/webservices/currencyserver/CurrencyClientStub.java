package com.currencysystem.webservices.currencyserver;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

public class CurrencyClientStub {
	
	public static CurrencyServerSoap getCurrencyServerSoap() throws ServiceException {
		return new CurrencyServerLocator().getCurrencyServerSoap();
	}
	
	public static void main(String[] args) throws ServiceException, RemoteException {
		CurrencyServerSoap service = new CurrencyServerLocator().getCurrencyServerSoap();
		
		System.out.println(service.activeCurrencies(""));
		System.out.println(service.convert("", "EUR", "USD", 1000, true, "", CurncsrvReturnRate.curncsrvReturnRateNumber, "", ""));
		//service.

		System.out.println(service.currencyExists("", "DJS", false));
	}
}
