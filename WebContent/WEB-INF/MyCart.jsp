<%@page import="fr.uge.rmi.selling.ifscarclient.Customer"%>
<%@page import="fr.uge.rmi.selling.ifscarclient.IfsCars"%>
<%@page import="fr.uge.rmi.selling.ifscarclient.IfsCarClientStub"%>
<%@page import="fr.uge.rmi.selling.ifscarclient.IfsCarClientStub"%>
<%@page import="fr.uge.rmi.cars.server.ICarServer"%>
<%@page import="fr.uge.rmi.cars.server.CarServer"%>
<%@page import="fr.uge.rmi.selling.ifscarclient.Car"%>
<%@page import="com.currencysystem.webservices.currencyserver.CurrencyClientStub"%>
<% 
	String username = null;
	String password = null;
	double sum = 0;
	Cookie[] cookies =  request.getCookies();
	for(int i = 0; i < cookies.length; i++){
		if (cookies[i].getName().equals("Username")){
			username = cookies[i].getValue(); 
		}
		else if (cookies[i].getName().equals("Password")){
			password = cookies[i].getValue(); 
		}
	}
	ICarServer carServer =  CarServer.getClient();
	IfsCars ifsCars = IfsCarClientStub.getClient();
	
	Car[] allCars = ifsCars.getCart(username, password);
	
	String[] currencies = CurrencyClientStub.getCurrencyServerSoap().activeCurrencies("").split(";");
	
%>
<!DOCTYPE html>
<html lang="fr" >

	<head>

		<meta name="DESCRIPTION" content="IfsCarsService the service where you can buy or locate a car." />
		<meta charset="UTF-8"/>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1" />

		<link rel="stylesheet" href="jquery-ui.css">
		
		<link rel="stylesheet" type="text/css" href="css/jquery.toast.css">
		<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
		<link rel="stylesheet" type="text/css" href="css/header.css">
		<link rel="stylesheet" type="text/css" href="css/meslocations.css">
	
		<script type="text/javascript" src="external/jquery/jquery.js"></script>
		<script type="text/javascript" src="jquery-ui.js"></script>
		<script type="text/javascript" src="js/jquery.toast.js"></script>
		<script type="text/javascript" src="js/CustomerUtils.js"></script>
		
		
		<title> Mon Panier </title>

	</head>

	<body>
		<div class="header-content">
				<h1 id="logo">
					<a class="logo_btn" href="customer">
						<img class="logo-picture" src="img/logo-cercle.png" alt="Logo du site IfsCar Service">
					</a>
				</h1>
				<nav id="nav" role="navigation">
					<ul id="main-menu">
						<li id="home">
							<a href="customer" >
								<i class="fas fa-home"></i> Accueil
							</a>
						</li>
						<li id="contact">
								<a href="mycars" >
								<i class="fas fa-car"></i> Mes voitures
								</a>
						</li>
						<li id="about_us">
							<a href="logout" >
							<i class="fas fa-sign-out-alt"></i> Se d&eacute;connecter
							</a>
						</li>
					</ul>
				</nav>
		</div>
		
		<% if(allCars == null || allCars.length == 0) { %>
			<div class="main-content">
				<h1 class="text" style="text-align: center;">Votre Panier est vide</h1>
			</div>
		<%} else { %>
			<div class="main-content">
				<h1 class="text" style="text-align: center;">Mon panier</h1>
				<div class="content-layout">
					<label for="devise-select">   Devise</label>
				<select id="currency" name="devises" id="devise-select">
				<% for (int i = 0; i < currencies.length; i++) { %>
					    <option value="<%=currencies[i]%>" <%=currencies[i].equals("EUR") ? "selected" : "" %>><%=currencies[i]%></option>
				<% } %>
				</select>
				<div id="total">Total: </div>
				<div><button onclick="payCart()">Pay</button></div>
			<br/>
			<br/>
					<table class="table">
	                    <thead class="thead">
	                        <tr>
	                            <th></th>
	                            <th style="text-align: center;">
	                                Mod&egrave;le
	                            </th>
	                            <th style="text-align: center;">
	                                Prix
	                            </th>
	                            <th style="text-align: center;">
	                            </th>
	                        </tr>
	                    </thead>
	                    <tbody class="tbody">
	                    
	                    <% for(int i = 0; i < allCars.length; i++) { %>
		                	<%
							Car car = allCars[i];
							long carId = car.getId();
							String model = car.getModel();
							sum += car.getValue();
						 	%>
	                        <tr>
	                            <td class="TDimg">
	                            	<div class="voitureImg">
										<img class="carStyle" src="img/cars/<%=car.getModel()%>.jpg">
									</div>
	                            </td>
	                            <td>
	                            	<div class="voitureInfos" style="text-align: center;"><%=car.getModel()%></div>
	                            </td>
	                            <td>
	                            	<div class="voitureInfos" id="car-price-<%=carId%>" style="text-align: center;"><%=car.getValue()%></div>
	                            </td>
	                            <td>
	                            	<div class="voitureActions">
										<a><button class="achat-button" id="car-<%=carId%>" type="button" onclick="removeCarToCart(<%=carId%>)"> Retirer </button></a>
									</div>
	                            </td>
	                        </tr>
					<% } %>
					</tbody>
					</table>
				</div>
				<% } %>
			</div>
			<script>
				startRefreshMessageDaemon();
				setListener();
				$("#total").append((<%=sum%>));
			</script>
	</body>
</html>