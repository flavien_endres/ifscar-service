<%@page import="fr.uge.rmi.employees.IEmployee"%>
<%@page import="fr.uge.rmi.employees.server.EmployeeServer"%>
<%@page import="fr.uge.rmi.cars.server.ICarServer"%>
<%@page import="fr.uge.rmi.cars.server.CarServer"%>
<%@page import="fr.uge.rmi.cars.Car"%>
<%@page import="fr.uge.rmi.cars.Mark"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% 
	String username = null;
	String password = null;
	Cookie[] cookies =  request.getCookies();
	for(int i = 0; i < cookies.length; i++){
		if (cookies[i].getName().equals("Username")){
			username = cookies[i].getValue(); 
		}
		else if (cookies[i].getName().equals("Password")){
			password = cookies[i].getValue(); 
		}
	}
	ICarServer carServer =  CarServer.getClient();
	IEmployee employee = EmployeeServer.getClient().connect(username, password);

	Car[] allCars = carServer.getAllCars();
%>
<!DOCTYPE HTML>
<html lang="fr" >

	<head>

		<meta name="DESCRIPTION" content="IfsCar Service | Achat de voitures a des prix renversants !"/>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1"/>

		<link rel="stylesheet" type="text/css" href="css/header.css">
		<link rel="stylesheet" type="text/css" href="css/homepage.css">
		<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
		<link rel="stylesheet" type="text/css" href="css/caroussel.css">

		<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>

		<title>IfsCar Service | Louez la voiture de vos rêves !</title>

	</head>
		
		<link rel="stylesheet" href="css/jquery.toast.css">
		
		<script type="text/javascript" src="js/jquery.toast.js"></script>
		<script type="text/javascript" src="js/EmployeeUtils.js"></script>
		
		<title>IfsCars Rental</title>

	</head>

	<body>
		<div class="web-content" role="banner">
			<div class="header-content">
				<header class="banner-header">
					<h1 id="logo">
						<a class="logo_btn" href="index.html">
							<img class="logo-picture" src="img/logo-cercle.png" alt="Logo du site IfsCar Service">
						</a>
					</h1>
					<nav id="nav" role="navigation">
						<ul id="main-menu">
							<li id="home">
								<a href="index.html" >
									<i class="fas fa-home"></i> Accueil
								</a>
							</li>
							<li id="contact">
								<a href="location" >
									<i class="fas fa-car"></i> Mes locations
								</a>
							</li>
							<li id="about_us">
								<a href="logout" >
								<i class="fas fa-sign-out-alt"></i> Se déconnecter
								</a>
							</li>
						</ul>
					</nav>
				</header>
			</div>
			<div class="main-content">
				<h1 style="margin-top:10%; text-align: center;"><%=employee.getUsername()%>, Louez la voiture de vos rêves !</h1>

				<div class="caroussel">

				<div class="caroussel_div">
				 <% for(int i = 0; i < allCars.length; i++) { %>
	                 <%
						Car car = allCars[i];
						Mark[] comments = carServer.getcarMarks(car);
						StringBuilder finalString = new StringBuilder(' ');
						long carId = car.getId();
						String availability = carServer.isAvailable(carId) ? "Disponible" : "En location";
						String model = car.getModel();
					 %>
				 	<div class="mySlides">
					  	<img id="mySlides_img" src="img/cars/<%=model%>.jpg" style="width:70%;">
					  	<div>
						  	<div id="slide-model-<%=carId%>" style="display:inline; border-width: 1px; border-style: dashed; border-color:black;"><%=model%></div>
						  	<div id="slide-status-<%=carId%>" style=" margin-left: 3px; display:inline; border-width: 1px; border-style: dashed; border-color:black;"><%=availability%></div>  
						  	<div id="button_validation"><a><button onclick="askForRental(<%=carId%>)">Je loue !</button></a></div>
					  	</div>
				  	</div>
				<% } %>
				<button class="caroussel_button_left" onclick="plusDivs(-1)">&#10094;</button>
			  	<button class="caroussel_button_right" onclick="plusDivs(1)">&#10095;</button>
				</div>
			</div>
			
			<h1 style="text-align:center;">Les voitures disponibles</h1>
			<div class="cars-list">
			  <table class="table">
                    <thead class="thead">
                        <tr>
                            <th style="text-align: center;">
                            </th>
                            <th style="text-align: center;">
                                Modèle
                            </th>
                            <th style="text-align: center;">
                            	Commentaires
                            </th>
                            <th style="text-align: center;">
                                Disponiblité
                            </th>
                            <th style="text-align: center;">
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                    <% for(int i = 0; i < allCars.length; i+=1) { %>
		                 <%
							Mark[] comments = carServer.getcarMarks(allCars[i]);
							StringBuilder finalString = new StringBuilder(' ');
							long carId = allCars[i].getId();
							Car car = allCars[i];
							String availability = carServer.isAvailable(carId) ? "Disponible" : "En location";
							String model = car.getModel();
						 %>
                        <tr>
                            <td class="TDimg">

                            	<div class="voitureImg">
									<img src="img/cars/<%=model%>.jpg">
								</div>
                            </td>
                            <td>
                            	<div style="text-align: center;" class="voitureInfos" id="car-model-<%=carId%>"><%=model%></div>
                            </td>
                            <td id="car-comment-<%=carId%>">
<%--                             	<div style="text-align: center;"><% for(int k = 0; k < comments.length; k++){ %><%= comments[k] %> <br><%}%></div> --%>
                            	<% for(int k = 0; k < comments.length; k++){ %><%= comments[k] %> <br><%}%>
                            </td>
                            <td>
                            	<div class="voitureDispo" id="status-<%=carId%>" style="text-align: center;" ><%=availability %></div>
                            </td>
                            <td>
                            	<div class="voitureActions">
									<a><button class="achat-button" id="car-<%=carId%>" type="button" onclick="askForRental(<%=carId%>)"> Je loue !</button></a>
								</div>
                            </td>
                        </tr>
				<% } %>
				</tbody>
				</table>
			</div>
		</div>
	</body>
	<script>
		startRefreshDisponibilityDaemon();
		startRefreshCommentsDaemon();
	
		//Sources : https://www.w3schools.com/w3css/w3css_slideshow.asp
		var slideIndex = 1;
		showDivs(slideIndex);

		function plusDivs(n) {
		  showDivs(slideIndex += n);
		}

		function showDivs(n) {
		  var i;
		  var x = document.getElementsByClassName("mySlides");
		  if (n > x.length) {slideIndex = 1}
		  if (n < 1) {slideIndex = x.length}
		  for (i = 0; i < x.length; i++) {
		    x[i].style.display = "none";  
		  }
		  x[slideIndex-1].style.display = "block";  
		}

		var myIndex = 0;
		carousel();

		function carousel() {
		  var i;
		  var x = document.getElementsByClassName("mySlides");
		  for (i = 0; i < x.length; i++) {
		    x[i].style.display = "none";  
		  }
		  myIndex++;
		  if (myIndex > x.length) {myIndex = 1}    
		  x[myIndex-1].style.display = "block";  
		  setTimeout(carousel, 5000); // Change image every 2 seconds
		}

	</script>
</html>