<%@page import="fr.uge.rmi.employees.server.EmployeeServer"%>
<%@page import="fr.uge.rmi.employees.server.IEmployeeServer"%>
<%@page import="fr.uge.rmi.employees.IEmployee"%>
<%@page import="fr.uge.rmi.cars.server.CarServer"%>
<%@page import="fr.uge.rmi.cars.server.ICarServer"%>
<%@page import="fr.uge.rmi.cars.Car"%>
<% 
	String username = null;
	String password = null;
	Cookie[] cookies =  request.getCookies();
	for(int i = 0; i < cookies.length; i++){
		if (cookies[i].getName().equals("Username")){
			username = cookies[i].getValue(); 
		}
		else if (cookies[i].getName().equals("Password")){
			password = cookies[i].getValue(); 
		}
	}
	ICarServer server =  CarServer.getClient();
	IEmployee employee = EmployeeServer.getClient().connect(username, password);
	
	Car car = employee.getCurrentCar();
%>
<!DOCTYPE html>
<html lang="fr" >

	<head>

		<meta name="DESCRIPTION" content="IfsCarsService the service where you can buy or locate a car." />
		<meta charset="UTF-8"/>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1" />

		<link rel="stylesheet" href="jquery-ui.css">
		
		<link rel="stylesheet" type="text/css" href="css/jquery.toast.css">
		<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
		<link rel="stylesheet" type="text/css" href="css/header.css">
		<link rel="stylesheet" type="text/css" href="css/meslocations.css">
	
		<script type="text/javascript" src="external/jquery/jquery.js"></script>
		<script type="text/javascript" src="jquery-ui.js"></script>
		<script type="text/javascript" src="js/jquery.toast.js"></script>
		<script type="text/javascript" src="js/EmployeeUtils.js"></script>
		
		
		<title> Mes locations </title>

	</head>

	<body>
		<div class="header-content">
				<h1 id="logo">
					<a class="logo_btn" href="index.html">
						<img class="logo-picture" src="img/logo-cercle.png" alt="Logo du site IfsCar Service">
					</a>
				</h1>
				<nav id="nav" role="navigation">
					<ul id="main-menu">
						<li id="home">
							<a onclick="javascript:history.back()" >
								<i class="fas fa-home"></i> Accueil
							</a>
						</li>
						<li id="contact">
							<a href="#" onclick="javascript:history.back()">
							<i class="fas fa-car"></i> Locations
							</a>
						</li>
						<li id="about_us">
							<a href="logout" >
							<i class="fas fa-sign-out-alt"></i> Se d&eacute;connecter
							</a>
						</li>
					</ul>
				</nav>
		</div>
		
		<% if(car == null) { %>
			<div class="main-content">
				<h1 class="text" style="text-align: center;">Aucune voiture n'est lou&eacute;</h1>
			</div>
		<%} else { %>
			<div class="main-content">
				<h1 class="text" style="text-align: center;">Mes voitures lou&eacute;es</h1>
				<div class="content-layout">
					<table class="table">
	                    <thead class="thead">
	                        <tr>
	                            <th></th>
	                            <th style="text-align: center;">
	                                Mod&egrave;le
	                            </th>
	                            <th style="text-align: center;">
	                                Actions
	                            </th>
	                        </tr>
	                    </thead>
	                    <tbody class="tbody">
	                        <tr>
	                            <td class="TDimg">
	                            	<div class="voitureImg">
										<img class="carStyle" src="img/cars/<%=car.getModel()%>.jpg">
									</div>
	                            </td>
	                            <td>
	                            	<div class="voitureInfos" style="text-align: center;"><%=car.getModel()%></div>
	                            </td>
	                            <td>
	                            	<div class="voitureActions">
										<a><button onclick="toggleShowDialogBox1()" class="return-button">Rendre la location</button></a>
									</div>
	                            </td>
	                        </tr>
					</tbody>
					</table>
				</div>
			</div>      
	<% } %>
	<div id = "boite_dialog">
	    <div>
	        <p> Souhaitez vous laisser un commentaire ? </p>
	        <button class = "boite_dialog_button" onclick="toggleShowDialogBox2()"> Oui </button>
	        <button class = "boite_dialog_button" onclick="returnCar(false)"> Non </button>
	    </div>
	   	<script type="text/javascript">
			function toggleShowDialogBox1(){
		         dialog_box = document.getElementById("boite_dialog");
		         dialog_box.style.visibility = (boite_dialog.style.visibility =="visible") ? "hidden" : "visible";
		    }
		</script>
	</div>
	<div id = "boite_dialog_yes">
    	<div>
	        <form id="markForm" class="rating">
	            <input id="star5" name="star" type="radio" value="5" class="radio-btn hide" />
	            <label for="star5" >&star;</label>
	            <input id="star4" name="star" type="radio" value="4" class="radio-btn hide" />
	            <label for="star4" >&star;</label>
	            <input id="star3" name="star" type="radio" value="3" class="radio-btn hide" />
	            <label for="star3" >&star;</label>
	            <input id="star2" name="star" type="radio" value="2" class="radio-btn hide" />
	            <label for="star2" >&star;</label>
	            <input id="star1" name="star" type="radio" value="1" class="radio-btn hide" />
	            <label for="star1" >&star;</label>
	        </form>
        <br/>
        <br/>
        <label for="comment"> Laissez-nous un commentaire </label>
        <br/>
        <textarea id ="comment" name="comment" rows ="5" cols="33">
            Ecrivez ici
        </textarea>
        <input type="submit" value="Validation" onclick="toggleShowDialogBox2(true)" />
        <button class = "boite_dialog_button" onclick="toggleShowDialogBox2(false)"> Close </button>
    </div>
    <script>
	    function toggleShowDialogBox2(send){
	        boite_dialog = document.getElementById("boite_dialog");
	        boite_commentaire = document.getElementById("comment");
	        if (send) {
	        	console.log("cant to return");
				returnCar(true); 
			}
	        $("#comment").val("");
	        boite_dialog_yes = document.getElementById("boite_dialog_yes");
	        (boite_dialog.style.visibility == "visible") ? toggleShowDialogBox1() : null
	        boite_dialog_yes.style.visibility = (boite_dialog_yes.style.visibility =="visible") ? "hidden" : "visible";
	    }
	</script>
</div>
	
	</body>
	
</html>