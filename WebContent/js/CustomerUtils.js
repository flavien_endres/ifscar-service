/**
 * 
 */
function refreshIfneeded(errorCode){
	console.log("error code " + errorCode);
	if(errorCode == 404){
		document.location.reload();
	}
}
	
function refreshDisponibility(){
	$.ajax({
	       method: "POST",
	       url : 'refreshdisponibility',
	       success: function(data){
				console.log("refreshDisponibility data");
				for (const [key, value] of Object.entries(data)) {
					var status = (`${value}` == 1 ? "Disponible" : "En location");
 					$("#slide-status-"+`${key}`).text(status);
 					$("#status-"+`${key}`).text(status);
				}
	       	},
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: 'carServer error',
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
		   }
	})
}

function changeCurrency(){
	$.ajax({
	       method: "POST",
	       url : 'changecurrency',
	       data : {
	       	Currency : ($("#currency").val())
	       },
	       success: function(data){
	       		total = 0;
				for (const [key, value] of Object.entries(data)) {
 					$("#slide-price-"+`${key}`).text(value);
 					if($("#car-price-"+`${key}`).length){
 						total += value;
 					}
 					$("#car-price-"+`${key}`).text(value);
// 					total += parseFloat(value);
				}
 					$("#total").text(("Total: " + total));
	       },
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: data,
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
		   }
	});
}

function setListener(){
	$("#currency").change(function(){changeCurrency();});

}

function startRefreshDisponibilityDaemon(){
	window.setInterval(refreshDisponibility, 2000);
}

function startRefreshMessageDaemon(){
	window.setInterval(getMessage, 1300);
}

function getMessage(){
	$.ajax({
	       method: "POST",
	       url : 'message',
	       success: function(data){
	       		if(data.length){
					$.toast({
		  			    heading: 'Success',
		  			    text: ('Message: ' + data),
		  			    showHideTransition: 'fade',
		  			    icon: 'success',
		  			    stack: 4
	  				});
	  			}
	       },
	});
}

function cartAction(id, mode){
	$.ajax({
	       method: "POST",
	       url : 'cartaction',
	       data : {
	       	"car-id" : id,
	       	"mode" : mode
	       },
	       success: function(data){
				$.toast({
	  			    heading: 'Success',
	  			    text: data,
	  			    showHideTransition: 'fade',
	  			    icon: 'success',
	  			    stack: 2
  				});
  				if(mode == "remove"){
	  				window.setTimeout(function(){document.location.reload();}, 4000);
  				}			
	       },
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: data.responseText,
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
  				refreshIfneeded(data.status);
		   }
	});
}


function addCarToCart(id){
	cartAction(id,"add");
}

function removeCarToCart(id){
	cartAction(id,"remove");
}

function payCart(){
		$.ajax({
	       method: "POST",
	       url : 'paycart',
	       success : function(){
	       	window.location.reload();
	       }
		});
}