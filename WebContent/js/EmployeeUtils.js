/**
 * 
 */
	
function askForRental(id){
	$.ajax({
       method: "POST",
       url : 'askrent',
	   data : {
			"car-id" : id,
			"car-model": ($(("#car-model-"+id)).text().trim())
	   },
       success: 
	       function(data){
				$.toast({
				    heading: 'Success',
				    text: data,
				    showHideTransition: 'fade',
			    	icon: 'success',
			    	stack: 2,
				})
	       },
       error: function(data){
				console.log(data);
		    	$.toast({
      			    heading: 'Error',
      			    text: data.responseText,
      			    showHideTransition: 'fade',
      			    icon: 'error',
      			    stack: 2
  				});
				refreshIfneeded(data.status)
		   },
		});
		
	};
	
function refreshIfneeded(errorCode){
	console.log("error code " + errorCode);
	if(errorCode == 404){
		document.location.reload();
	}
}
	
function refreshDisponibility(){
	$.ajax({
	       method: "POST",
	       url : 'refreshdisponibility',
	       success: function(data){
				console.log("refreshDisponibility data");
				for (const [key, value] of Object.entries(data)) {
					var status = (`${value}` == 1 ? "Disponible" : "En location");
 					$("#slide-status-"+`${key}`).text(status);
 					$("#status-"+`${key}`).text(status);
				}
	       	},
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: 'carServer error',
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
		   }
	})
}

function refreshDisponibility(){
	$.ajax({
	       method: "POST",
	       url : 'refreshdisponibility',
	       success: function(data){
				console.log("refreshDisponibility data");
				for (const [key, value] of Object.entries(data)) {
					var status = (`${value}` == 1 ? "Disponible" : "En location");
 					$("#slide-status-"+`${key}`).text(status);
 					$("#status-"+`${key}`).text(status);
				}
	       	},
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: 'carServer error',
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
		   }
	})
}

function refreshComments(){
	$.ajax({
	       method: "POST",
	       url : 'refreshcomments',
	       success: function(data){
				console.log("refreshcomments data");
				console.log(data);
				for (const [key, value] of Object.entries(data)) {
					m = false;
					finalT = "";
					if(value.length != 0){
						$("#car-comment-"+`${key}`).text('')
					}
					value.forEach(function(c){
 						$("#car-comment-"+`${key}`).append((c) +'<br>');
					});
				}
	       	},
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: 'carServer error',
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
		   }
	})
}

function returnCar(withComment){
	var requestParameters = {};
	
	if(withComment){
		requestParameters = {
			mark : $("input:radio[name=star]:checked").val() || 5,
			comment: $("#comment").val()
		}
	}
//	console.log("requestParameters");
//	console.log(requestParameters);
   $.ajax({
	       method: "POST",
	       url : 'returncar',
	       data : requestParameters,
	       success: function(data){
	       		console.log(data.responseText);
	       		console.log(data);
       			$.toast({
		  			    heading: 'Success',
		  			    text: data,
		  			    showHideTransition: 'fade',
		  			    icon: 'success',
		  			    stack: 2,
		  			    hideAfter:1000
				});
  				window.setTimeout(function(){document.location.reload();}, 1000);
	       },
	       error: function(data){
		    	$.toast({
	  			    heading: 'Error',
	  			    text: data.responseText,
	  			    showHideTransition: 'fade',
	  			    icon: 'error',
	  			    stack: 2
  				});
		   }
	});
}

function startRefreshCommentsDaemon(){
	window.setInterval(refreshComments, 5000);
}

function startRefreshDisponibilityDaemon(){
	window.setInterval(refreshDisponibility, 2000);
}

